package Ex1;

class A{

}

public class B extends A{
    D d;

    public void setD(D d){
        this.d = d;
    }

}

class C{

}

class D implements Y{
    E e;
    F f;
    public D(F f){
        this.f = f;
        e = new E();
    }

}

class E{

}

class F{

}

interface Y{