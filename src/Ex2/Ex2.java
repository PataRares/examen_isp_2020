package Ex2;

import javax.swing.*;

public class Ex2 extends JFrame {
    JTextField x;
    JTextField y;
    JButton B;
    Ex2(){
        setTitle("Titlu");
        init();
        setSize(200,200);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public void init(){
        this.setLayout(null);
        int w = 150;
        int h = 30;
        x = new JTextField();
        x.setBounds(5,10,w,h);
        y = new JTextField();
        y.setBounds(5,41,w,h);
        B = new JButton("apasa");
        B.setBounds(30,80,50,h);
        add(x);
        add(y);
        add(B);
    }
    public static void main(String[] args){
        new Ex2();
    }
}
